package com.iafanasiev.validator;

import com.iafanasiev.exception.NullOrEmptyPropertiesException;
import com.iafanasiev.exception.OutOfRangeForTypeException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesValidatorTest {
    @ParameterizedTest
    @CsvSource({
            "byte,-129, 127, 127",
            "byte, 0, 128, 1",
            "byte, 1, 1, 128",
            "byte, 1.0, -100, 1",
            "byte, 1.0, -100, 1",
            "byte, 1.0, 100, 0",

            "short, -32769, 32767, 1",
            "short, 0, 32768, 1",
            "short, 1, 1, 32768",
            "short, 1.0, -100, 1",
            "short, 1.0, 100, 0",

            "int, -2147483649, 2147483647, 1",
            "int, 0, 2147483648, 1",
            "int, 1, 1, 2147483648",
            "int, 1.0, -100, 1",
            "int, 1.0, 100, 0",

            "long, -9223372036854775809, 9223372036854775807, 1",
            "long, 0, 9223372036854775808, 1",
            "long, 1, 1, 9223372036854775808",
            "long, 1.0, 100, 1",
            "long, 1.0, -100, 1",
            "long, 1.0, 100, 0",

            "float, -3.5e+38, 3.4028235e+, 1.0",
            "float, 0, 3.5e+38, 1.0",
            "float, 1.0, 1.0, 3.5e+38",
            "float, 100, -100, 1",
            "float, 1, -100, 1",
            "float, 1.0, 100.0, 0",

            "double, -1.8e+308, 1.7976931348623157e+308, 1.0",
            "double, 0, 1.8e+308, 1.0",
            "double, 1.0, 1.0, 1.8e+308",
            "double, 1, -100, 1",
            "double, 1, -100, 1",
            "double, 1.0, 100.0, 0"
    })
    void negativeValidateRangesForTypeTest(String type, String minStr, String maxStr, String incrementStr) {
        assertThrows(IllegalArgumentException.class, () -> PropertiesValidator.validate(type, minStr, maxStr, incrementStr));
    }

    @ParameterizedTest
    @CsvSource({
            "byte, , -100, 1",
            "byte, '', -100, 1",
            "byte, 1.0, , 0"
    })
    void nullOrEmptyPropertiesTest(String type, String minStr, String maxStr, String incrementStr) {

        assertThrows(NullOrEmptyPropertiesException.class, () -> PropertiesValidator.validate(type, minStr, maxStr, incrementStr));
    }

    @ParameterizedTest
    @CsvSource({
            "byte, 1.0, 12, 0",
            "byte, -129, 127n, 0",
            "byte, '127,9', 127, 12",
            "short, 12e, 1, 1",
            "short, 32767.0, 32767, 1",
            "short, '1,0',32768, 2",
            "int, '12,8', 1, 1",
            "int, 1, 15yu, 1",
            "int, 12.0, 16, 1",
            "long, -e4, 1, 1",
            "long, -1, 1.0, 1",
            "long, 1, 9223372036854775808, 1",
            "long, 1, 1, 12df",
            "long, 1, '1,8', -9223372036854775809",
            "float, -3, '1,0', 1",
            "float, 34E, 1, 1",
            "float, 34, 1, @d1",
            "double, -1yu, 1, 1",
            "double, -1.7976931348623158E, '1,5', 1",
            "double, 1, jdjkk, 1",
            "double, 1@, -1.7976931348623157e+308, 1",
    })
    void negativeFormatForTypeTest(String type, String min, String max, String increment) {
        assertThrows(NumberFormatException.class, () -> PropertiesValidator.validateNumsComplianceForType(min, max, increment, type));
    }

    @ParameterizedTest
    @CsvSource({
            "byte, -128, 128, 0",
            "byte, -129, 127, 0",
            "byte, 127, 127, 128",
            "byte, 0, 0, -129",
            "short, -32769, 1, 1",
            "short, 32768, 1, 1",
            "short, 1,32768, 1",
            "short, 1,-32769, 1",
            "short, 1, 1, 32768",
            "short, 1, 1, -32769",
            "int, -2147483649, 1, 1",
            "int, 2147483648, 1, 1",
            "int, 1, 2147483648, 1",
            "int, 1, -2147483649, 1",
            "int, 1, 1, 2147483648",
            "int, 1, 1, -2147483649",
            "long, -9223372036854775809, 1, 1",
            "long, 9223372036854775808, 1, 1",
            "long, 1, 9223372036854775808, 1",
            "long, 1, -9223372036854775809, 1",
            "long, 1, 1, 9223372036854775808",
            "long, 1, 1, -9223372036854775809",
            "float, -3.4028236E38, 1, 1",
            "float, 3.4028236E38, 1, 1",
            "float, 1, 3.4028236E38, 1",
            "float, 1, -3.4028236E38, 1",
            "float, 3.40282346638528860e+39, 1, 1",
            "float, 1, -3.40282346638528860e+39, 1",
            "float, 1, 1, 3.4028236E38",
            "float, 1, 1, -3.4028236E38",
            "double, -1.7976931348623158E308, 1, 1",
            "double, 1.7976931348623158E308, 1, 1",
            "double, 1, 1.7976931348623157e+309, 1",
            "double, 1, -1.7976931348623157e+309, 1",
            "double, 1, 1, 1.7976931348623158e+308",
            "double, 1, 1, -1.7976931348623158E308"
    })
    void negativeRangeTest(String type, BigDecimal min, BigDecimal max, BigDecimal increment) {
        PropertiesValidator.min = min;
        PropertiesValidator.max = max;
        PropertiesValidator.increment = increment;
        assertThrows(OutOfRangeForTypeException.class, () -> PropertiesValidator.validateRangesForType(type));
    }

    @ParameterizedTest
    @CsvSource({
            "byte, 0, 127, 1",
            "byte, -128, 127, 1",
            "byte, 127, 127, 1",
            "short, -32768, 32767, 1",
            "short, 0, 32767, 1",
            "int, -2147483648, 2147483647, 1",
            "int, 0, 2147483647, 1",
            "long, -9223372036854775808, 9223372036854775807, 1",
            "long, 0, 9223372036854775807, 1",
            "float,-3.40282346638528860e+38, 3.40282346638528860e+38, 1.0",
            "float, 0.0, 3.40282346638528860e+38, 1.0",
            "double, 0, 1.7976931348623157e+308, 1.0",
            "double, 4.9406564584124654e-324, -1.7976931348623157e+308, 1.0"
    })
    void positiveRangesTest(String type, String min, String max, String increment) {

        assertDoesNotThrow(() -> PropertiesValidator.validateNumsComplianceForType(min, max, increment, type));
    }

    @ParameterizedTest
    @CsvSource({
            ", 127, 1, min",
            "'', 127, 1, min",
            "-128, , 1, max",
            "-128, '', 1, max",
            "-128, 127,, increment",
            "-128, 127,'', increment",
            ",,,'min, max, increment'",
            "'','','','min, max, increment'",
    })
    void validatePropertiesNotEmptyTest(String min, String max, String increment, String missedProperties) {
        NullOrEmptyPropertiesException exception = assertThrows(NullOrEmptyPropertiesException.class, () -> PropertiesValidator.validatePropertiesNotEmpty(min, max, increment));

        assertEquals("Missing or empty properties detected: " + missedProperties, exception.getMessage());
    }

    @ParameterizedTest
    @CsvSource({
            "-128, 127, 12",
            "1.7976931348623157e+308, 1.7976931348623157e+308, 1",
            "-128, 54, 1",
            "33,4,4",
    })
    void positivePropertiesNotEmptyTest(String min, String max, String increment) {
        assertDoesNotThrow(() -> PropertiesValidator.validatePropertiesNotEmpty(min, max, increment));
    }

    @ParameterizedTest
    @CsvSource({
            "10, 5, 1,\"min\" must be less or equal to \"max\"",             // min > max
            "5, 5, -1,\"increment\" must be more than zero",             // increment <= 0
            "1.7976931348623157e+308, 5, 1,\"min\" must be less or equal to \"max\"",   // min > max

    })
    void negativePropertiesLogicTest(BigDecimal min, BigDecimal max, BigDecimal increment, String expMsg) {
        PropertiesValidator.min = min;
        PropertiesValidator.max = max;
        PropertiesValidator.increment = increment;

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, PropertiesValidator::validatePropertiesLogic);

        assertEquals(expMsg, exception.getMessage());
    }

    @ParameterizedTest
    @CsvSource({
            "5, 10, 1",
            "-500, 10, 0.0001",
    })
    void positivePropertiesLogicTest(BigDecimal min, BigDecimal max, BigDecimal increment) {
        PropertiesValidator.min = min;
        PropertiesValidator.max = max;
        PropertiesValidator.increment = increment;

        assertDoesNotThrow(PropertiesValidator::validatePropertiesLogic);
    }

    private static Stream<Arguments> provideIsNotWithinRangeTestCases() {
        return Stream.of(
                Arguments.of("double", new BigDecimal("1.7976931348623157e+308"), false),
                Arguments.of("double", new BigDecimal("1.7976931348623157E308"), false),
                Arguments.of("double", new BigDecimal("1.7976931348623157e+309"), true),//out
                Arguments.of("double", new BigDecimal("1.7976931348623157E309"), true),//out

                Arguments.of("double", new BigDecimal("-1.7976931348623157e+308"), false),
                Arguments.of("double", new BigDecimal("-1.7976931348623157E308"), false),
                Arguments.of("double", new BigDecimal("-1.7976931348623157e+309"), true),//out
                Arguments.of("double", new BigDecimal("-1.7976931348623157E309"), true),//out

                Arguments.of("double", new BigDecimal("-70"), false),
                Arguments.of("double", new BigDecimal("0"), false),
                Arguments.of("double", new BigDecimal("0.0"), false),

                Arguments.of("float", new BigDecimal("3.40282346638528860e+38"), false),
                Arguments.of("float", new BigDecimal(Float.MAX_VALUE), false),
                Arguments.of("float", new BigDecimal("3.40282346638528860E38"), false),
                Arguments.of("float", new BigDecimal("3.4028234E38"), false),
                Arguments.of("float", new BigDecimal("3.4028235e+38"), false),
                Arguments.of("float", new BigDecimal("3.4028235E39"), true), // out

                Arguments.of("float", new BigDecimal("-3.40282346638528860e+38"), false),
                Arguments.of("float", new BigDecimal("-3.40282346638528860E38"), false),
                Arguments.of("float", BigDecimal.valueOf(-Float.MAX_VALUE), false),
                Arguments.of("float", new BigDecimal("-3.4028235e+39"), true), // out
                Arguments.of("float", new BigDecimal("-3.4028235E39"), true), // out

                Arguments.of("float", new BigDecimal(-70), false),
                Arguments.of("float", new BigDecimal(0), false),
                Arguments.of("float", new BigDecimal("0.0"), false),

                // Byte
                Arguments.of("byte", BigDecimal.valueOf(Byte.MAX_VALUE), false),
                Arguments.of("byte", BigDecimal.valueOf(Byte.MIN_VALUE), false),
                Arguments.of("byte", BigDecimal.valueOf(Byte.MAX_VALUE + 1), true), // out
                Arguments.of("byte", BigDecimal.valueOf(Byte.MIN_VALUE - 1), true), // out

                // Short
                Arguments.of("short", BigDecimal.valueOf(Short.MAX_VALUE), false),
                Arguments.of("short", BigDecimal.valueOf(Short.MIN_VALUE), false),
                Arguments.of("short", BigDecimal.valueOf(Short.MAX_VALUE + 1), true), // out
                Arguments.of("short", BigDecimal.valueOf(Short.MIN_VALUE - 1), true), // out

                // Int
                Arguments.of("int", BigDecimal.valueOf(Integer.MAX_VALUE), false),
                Arguments.of("int", BigDecimal.valueOf(Integer.MIN_VALUE), false),
                Arguments.of("int", BigDecimal.valueOf((long) Integer.MAX_VALUE + 1), true), // out
                Arguments.of("int", BigDecimal.valueOf((long) Integer.MIN_VALUE - 1), true), // out

                // Long
                Arguments.of("long", BigDecimal.valueOf(Long.MAX_VALUE), false),
                Arguments.of("long", BigDecimal.valueOf(Long.MIN_VALUE), false),
                Arguments.of("long", BigDecimal.valueOf(Long.MAX_VALUE).add(BigDecimal.ONE), true),
                Arguments.of("long", BigDecimal.valueOf(Long.MIN_VALUE).subtract(BigDecimal.ONE), true)
        );
    }

    @ParameterizedTest
    @MethodSource("provideIsNotWithinRangeTestCases")
    void isOutOfRangeForType(String type, BigDecimal value, boolean expected) {
        boolean actualResult = PropertiesValidator.isOutOfRangeForType(type, value);
        assertEquals(expected, actualResult, "val: " + value + ": " + Float.MAX_VALUE);
    }
}