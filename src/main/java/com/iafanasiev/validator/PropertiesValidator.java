package com.iafanasiev.validator;

import com.iafanasiev.exception.NullOrEmptyPropertiesException;
import com.iafanasiev.exception.OutOfRangeForTypeException;
import com.iafanasiev.util.TypeRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.iafanasiev.parser.NumericParser.parseValueTo;

public class PropertiesValidator {

    static Logger logger = LoggerFactory.getLogger(PropertiesValidator.class);
    static BigDecimal min;
    static BigDecimal max;
    static BigDecimal increment;

    public static void validate(String type, String minStr, String maxStr, String incrementStr) {
        logger.info("Start validation of parameters for the multiplication table");
        validatePropertiesNotEmpty(minStr, maxStr, incrementStr);

        validateNumsComplianceForType(minStr, maxStr, incrementStr, type);
        setBigDecimalValues(minStr, maxStr, incrementStr);

        validateRangesForType(type);

        validatePropertiesLogic();
    }

    static void validatePropertiesNotEmpty(String min, String max, String increment) {
        logger.debug("Validate that properties are not empty");
        List<String> invalidProperties = new ArrayList<>();
        boolean propertiesEmpty = false;
        if (min == null || min.isEmpty()) {
            propertiesEmpty = true;
            invalidProperties.add("min");
        }
        if (max == null || max.isEmpty()) {
            propertiesEmpty = true;
            invalidProperties.add("max");
        }
        if (increment == null || increment.isEmpty()) {
            propertiesEmpty = true;
            invalidProperties.add("increment");
        }
        if (propertiesEmpty) {
            throw new NullOrEmptyPropertiesException("Missing or empty properties detected: " + String.join(", ", invalidProperties));
        }
    }

    static void validateNumsComplianceForType(String minStr, String maxStr, String incrementStr, String type) {
        parseValueTo(minStr, type);
        parseValueTo(maxStr, type);
        parseValueTo(incrementStr, type);
    }

    static void validatePropertiesLogic() {
        logger.debug("Validate logic of properties values");
        if (!isLessOrEqualTo(min, max)) {
            throw new IllegalArgumentException("\"min\" must be less or equal to \"max\"");
        }
        if (!isPositive(increment)) {
            logger.error("increment must be positive");
            throw new IllegalArgumentException("\"increment\" must be more than zero");
        }
    }

    public static void validateRangesForType(String type) {
        logger.debug("Validate ranges of properties values for the given type: {}", type);

        List<String> invalidValuesRange = new ArrayList<>();
        boolean isValidRange = true;
        if (isOutOfRangeForType(type, min)) {
            invalidValuesRange.add("min");
            isValidRange = false;
        }
        if (isOutOfRangeForType(type, max)) {
            invalidValuesRange.add("max");
            isValidRange = false;
        }
        if (isOutOfRangeForType(type, increment)) {
            invalidValuesRange.add("increment");
            isValidRange = false;
        }
        if (!isValidRange) {
            throw new OutOfRangeForTypeException(type, invalidValuesRange);
        }
    }

    public static boolean isOutOfRangeForType(String type, BigDecimal value) {
        TypeRange range = TypeRange.fromString(type);
        return value.compareTo(range.getMin()) < 0 || value.compareTo(range.getMax()) > 0;
    }


    private static boolean isLessOrEqualTo(BigDecimal min, BigDecimal max) {
        return min.compareTo(max) <= 0;
    }

    private static boolean isPositive(BigDecimal num) {
        return num.compareTo(BigDecimal.ZERO) > 0;
    }

    private static void setBigDecimalValues(String min, String max, String increment) {
        PropertiesValidator.min = new BigDecimal(min);
        PropertiesValidator.max = new BigDecimal(max);
        PropertiesValidator.increment = new BigDecimal(increment);

    }

    private PropertiesValidator() {
    }
}