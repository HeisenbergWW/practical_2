package com.iafanasiev.dto;

import com.iafanasiev.util.TypeRange;

import java.math.BigDecimal;

public class MultiplicationTableDTO {
    private final BigDecimal min;
    private final BigDecimal max;
    private final BigDecimal increment;
    final TypeRange range;
    private final String type;

    public MultiplicationTableDTO(String type, BigDecimal min, BigDecimal max, BigDecimal increment, TypeRange typeRanges) {
        this.type = type;
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.range = typeRanges;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public BigDecimal getIncrement() {
        return increment;
    }

    public BigDecimal getMinRange() {
        return range.getMin();
    }

    public BigDecimal getMaxRange() {
        return range.getMax();
    }
}