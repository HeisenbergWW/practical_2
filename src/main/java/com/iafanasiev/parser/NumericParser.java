package com.iafanasiev.parser;

import com.iafanasiev.exception.UnsupportedDataTypeException;

import java.math.BigDecimal;

public class NumericParser {

    public static BigDecimal parseValueTo(String value, String type) {

        return switch (type.toLowerCase()) {
            case "byte" -> new BigDecimal(Byte.valueOf(value).toString());
            case "short" -> new BigDecimal(Short.valueOf(value).toString());
            case "int" -> new BigDecimal(Integer.valueOf(value).toString());
            case "long" -> new BigDecimal(Long.valueOf(value).toString());
            case "float" -> new BigDecimal(Float.valueOf(value).toString());
            case "double" -> new BigDecimal(Double.valueOf(value).toString());
            default -> throw new UnsupportedDataTypeException("Unsupported type: " + type);
        };
    }

    NumericParser() {
        throw new IllegalStateException("Utility class");
    }
}