package com.iafanasiev;

import com.iafanasiev.dto.MultiplicationTableDTO;
import com.iafanasiev.exception.UnsupportedDataTypeException;
import com.iafanasiev.util.MultiplicationTableDTOFactory;
import com.iafanasiev.util.TypeRange;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTableTest {

    @ParameterizedTest
    @CsvSource({
            "byt,3 , -100, 1",
            "byt,-1 , 100, 1",
            "byt, 1.0, 3, 0"
    })
    void invalidTypePropertiesTest(String type, String minStr, String maxStr, String incrementStr) {
        assertThrows(UnsupportedDataTypeException.class, () -> MultiplicationTableDTOFactory.buildTableParamsDTO(type, minStr, maxStr, incrementStr));
    }

    @ParameterizedTest
    @CsvSource({
            "byte,-128, 127, 127",
            "byte, -128, -128, 1",
            "short, 0, 32767, 1",
            "int, 0, 2147483647, 1",
            "long, 0, 9223372036854775807, 1",
            "float, 0.0, 3.4028233E+38, 1.0",
            "float, -3.4028233E38, 3.4028233E38, 1.0",
            "double, 0.0, 1.7976931348623157E308, 1.0"
    })
    void positiveBuildTableParamsStructTest(String type, String minStr, String maxStr, String incrementStr) {
        MultiplicationTableDTO params = MultiplicationTableDTOFactory.buildTableParamsDTO(type, minStr, maxStr, incrementStr);

        BigDecimal min = new BigDecimal(minStr);
        BigDecimal max = new BigDecimal(maxStr);
        BigDecimal increment = new BigDecimal(incrementStr);
        TypeRange range = TypeRange.fromString(type);

        assertEquals(type, params.getType());
        assertEquals(min, params.getMin());
        assertEquals(max, params.getMax());
        assertEquals(increment, params.getIncrement());
        assertEquals(range.getMin(), params.getMinRange());
        assertEquals(range.getMax(), params.getMaxRange());
    }

    @ParameterizedTest
    @CsvSource({
            "byte, 127, 127, 1, 127 * 127 = outOfRange",
            "byte, 2, 64, 62, 2 * 64 = outOfRange",
            "short, 32767, 32767, 1, 32767 * 32767 = outOfRange",
            "int, 2147483647, 2147483647, 1, 2147483647 * 2147483647 = outOfRange |",
            "long, 9223372036854775807, 9223372036854775807, 1, 9223372036854775807 * 9223372036854775807 = outOfRange",
            "float, 3.4028234663852886E+38, 3.4028234663852886E+38, 1, 3.4028234663852886E+38 * 3.4028234663852886E+38 = outOfRange",
            "double, 1.7976931348623157e+308, 1.7976931348623157e+308, 1, 1.7976931348623157e+308 * 1.7976931348623157e+308 = outOfRange"
    })
    void printTableTest(String type, BigDecimal min, BigDecimal max, BigDecimal increment, String expectedOutput) {
        try {
            MultiplicationTableDTO params =
                    new MultiplicationTableDTO(
                            type,
                            min,
                            max,
                            increment,
                            TypeRange.fromString(type)
                    );

            String output = tapSystemOut(() -> MultiplicationTable.printTable(params));

            String sanitizedOutput = output.replaceAll("\\s+", "").toLowerCase();
            String sanitizedExpectedOutput = expectedOutput.replaceAll("\\s+", "").toLowerCase();


            assertTrue(sanitizedOutput.contains(sanitizedExpectedOutput));
        } catch (Exception e) {
            fail("An exception occurred: " + e.getMessage());
        }
    }
}