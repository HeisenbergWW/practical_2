package com.iafanasiev.util;

import com.iafanasiev.exception.UnsupportedDataTypeException;

import java.math.BigDecimal;

public enum TypeRange {
    BYTE(BigDecimal.valueOf(Byte.MIN_VALUE), BigDecimal.valueOf(Byte.MAX_VALUE)),
    SHORT(BigDecimal.valueOf(Short.MIN_VALUE), BigDecimal.valueOf(Short.MAX_VALUE)),
    INT(BigDecimal.valueOf(Integer.MIN_VALUE), BigDecimal.valueOf(Integer.MAX_VALUE)),
    LONG(BigDecimal.valueOf(Long.MIN_VALUE), BigDecimal.valueOf(Long.MAX_VALUE)),
    FLOAT(new BigDecimal("-3.4028235e+38"), new BigDecimal("3.4028235e+38")),
    DOUBLE(BigDecimal.valueOf(-Double.MAX_VALUE), BigDecimal.valueOf(Double.MAX_VALUE));

    private final BigDecimal min;
    private final BigDecimal max;

    TypeRange(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public static TypeRange fromString(String type) {
        try {
            return TypeRange.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new UnsupportedDataTypeException("Unsupported data type: \"" + type + "\"");
        }
    }
}