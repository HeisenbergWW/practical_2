package com.iafanasiev.exception;

import java.util.List;

public class OutOfRangeForTypeException extends RuntimeException {
    public OutOfRangeForTypeException(String type, List<String> values) {
        super(String.format("Values: %s are out of range for type: %s", String.join(", ", values), type));
    }
}