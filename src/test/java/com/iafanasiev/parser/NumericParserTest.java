package com.iafanasiev.parser;

import com.iafanasiev.exception.UnsupportedDataTypeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NumericParserTest {
    @ParameterizedTest
    @CsvSource({
            "byte, '127', 127",
            "BYTE, '-128', -128",
            "byte, '-128', -128",
            "byte, '0', 0",
            "short, '32767', 32767",
            "short, '-32768', -32768",
            "short, '0', 0",
            "int, '2147483647', 2147483647",
            "int, '-2147483648', -2147483648",
            "int, '0', 0",
            "long, '9223372036854775807', 9223372036854775807",
            "long, '-9223372036854775808', -9223372036854775808",
            "long, '0', 0",
            "fLoAt, '-1.40129846432481707e-45', -1.4E-45",
            "float, '3.40282346638528860e+38', 3.4028235E+38",
            "float, '0', 0",
            "float, '2.40282346638528860e+38', 2.4028234E+38",
            "float, '1.23456789', 1.2345679",
            "double, '4.94065645841246544e-324', 4.9E-324",
            "double, '1.79769313486231570e+308', 1.79769313486231570e+308",
            "double, '9.87654321', 9.87654321"
    })
    void parseValueToTest(String type, String value, BigDecimal expected) {
        BigDecimal parsedValue = NumericParser.parseValueTo(value, type);
        assertEquals(0, parsedValue.compareTo(expected), "Parsed value does not match expected " + parsedValue);
    }

    @ParameterizedTest
    @CsvSource({
            "byte, '127.0'",
            "short, '12.0'",
            "long, '123.0'",
            "int, '1.0'",
            "float, '12..'",
            "float, 'i'",
            "float, 'zxc'",
            "double, '55.5.5'"
    })
    void negativeFormatPareValueTest(String type, String value) {
        assertThrows(NumberFormatException.class, () -> NumericParser.parseValueTo(value, type), "Invalid value should throw NumberFormatException");
    }

    @ParameterizedTest
    @CsvSource({
            "XXX, '127.0'",
            "doudfdble, '55.5.5'"
    })
    void negativeTypePareValueTest(String type, String value) {
        assertThrows(UnsupportedDataTypeException.class, () -> NumericParser.parseValueTo(value, type), "Invalid type should throw UnsupportedDataTypeException");
    }

    @Test
    void createPrivateConstructorTest() {
        assertThrows(IllegalStateException.class, NumericParser::new);
    }
}
