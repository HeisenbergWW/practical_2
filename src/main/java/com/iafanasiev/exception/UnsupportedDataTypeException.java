package com.iafanasiev.exception;

public class UnsupportedDataTypeException extends RuntimeException {
    public UnsupportedDataTypeException(String message) {
        super(message);
    }
}