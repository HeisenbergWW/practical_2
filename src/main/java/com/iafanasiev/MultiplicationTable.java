package com.iafanasiev;

import com.iafanasiev.dto.MultiplicationTableDTO;
import com.iafanasiev.exception.NullOrEmptyPropertiesException;
import com.iafanasiev.exception.UnsupportedDataTypeException;
import com.iafanasiev.util.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import java.util.Properties;

import static com.iafanasiev.util.MultiplicationTableDTOFactory.buildTableParamsDTO;
import static com.iafanasiev.validator.PropertiesValidator.isOutOfRangeForType;
import static com.iafanasiev.validator.PropertiesValidator.validate;

public class MultiplicationTable {
    static Logger logger = LoggerFactory.getLogger(MultiplicationTable.class);
    static Logger loggerTable = LoggerFactory.getLogger("tableLOGER");
    static final String PROPERTIES_FILE_NAME = "app.properties";

    public static void main(String[] args) {
        logger.info("Start of program");

        String type = System.getProperty("type", "int");

        logger.info("Was selected type: {}", type);

        try {
            Properties properties = PropertiesLoader.loadProperties(PROPERTIES_FILE_NAME);

            String minStr = properties.getProperty("min");
            String maxStr = properties.getProperty("max");
            String incrementStr = properties.getProperty("increment");
            logger.info("Was got such string values min: {}, max: {}, increment: {}", minStr, maxStr, incrementStr);

            validate(type, minStr, maxStr, incrementStr);

            MultiplicationTableDTO tableParamsStruct = buildTableParamsDTO(type, minStr, maxStr, incrementStr);

            printTable(tableParamsStruct);
        } catch (NumberFormatException e) {
            logger.error("Ensure that values have the valid format for the selected type \"{}\"."
                    , type, e);
        } catch (IllegalArgumentException | UnsupportedDataTypeException | FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (NullOrEmptyPropertiesException e) {
            logger.error("In properties file: '{}'. {}", PROPERTIES_FILE_NAME, e.getMessage(), e);
        } catch (IOException e) {
            logger.error("Unknown problem, reload PC and try later :-) {}", e.getMessage(), e);
        }
        logger.info("End of program");
    }

    static void printTable(MultiplicationTableDTO tableParameters) {
        logger.info("Start print multiplication table:");
        BigDecimal multiplierWidth = calculateMaxMultiplierWidth(tableParameters);

        String outOfRangeMsg = "outOfRange";
        BigDecimal maxResultWidth = calculateMaxResultWidth(tableParameters.getMax(), tableParameters.getMinRange(), tableParameters.getMaxRange(),
                outOfRangeMsg);


        for (BigDecimal i = tableParameters.getMin(); i.compareTo(tableParameters.getMax()) <= 0; i = i.add(tableParameters.getIncrement())) {
            for (BigDecimal j = tableParameters.getMin(); j.compareTo(tableParameters.getMax()) <= 0; j = j.add(tableParameters.getIncrement())) {
                BigDecimal product = i.multiply(j);

                String result = isOutOfRangeForType(tableParameters.getType(), product) ? outOfRangeMsg : product.toString();
                String format = "%" + multiplierWidth + "s * %" + multiplierWidth + "s = %" + maxResultWidth + "s | ";

                var msg = String.format(format, j, i, result);

                loggerTable.info("{}", msg);
            }
            loggerTable.info("\n");
        }
    }

    private static BigDecimal calculateMaxMultiplierWidth(MultiplicationTableDTO tableParameters) {
        BigDecimal maxValueLength = BigDecimal.valueOf(tableParameters.getMax().toString().length());
        return tableParameters.getMin().compareTo(BigDecimal.valueOf(0)) >= 0 ?
                maxValueLength :
                maxValueLength.add(BigDecimal.valueOf(1));
    }

    private static BigDecimal calculateMaxResultWidth(BigDecimal maxMultiplier, BigDecimal minRange, BigDecimal maxRange,
                                                      String outOfRangeMsg) {
        BigDecimal maxResult = maxMultiplier.multiply(maxMultiplier);

        BigDecimal maxResultLength = BigDecimal.valueOf(String.valueOf(maxResult).length());

        if (minRange.compareTo(BigDecimal.ZERO) < 0) {
            maxResultLength = maxResultLength.add(BigDecimal.ONE);
        }

        if (maxResult.compareTo(minRange) < 0 ||
                maxResult.compareTo(maxRange) > 0) {
            return BigDecimal.valueOf(Math.max(outOfRangeMsg.length(), maxResultLength.intValue()));
        } else {
            return maxResultLength;
        }
    }
}