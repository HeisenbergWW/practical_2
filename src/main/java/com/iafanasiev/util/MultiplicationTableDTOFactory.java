package com.iafanasiev.util;

import com.iafanasiev.dto.MultiplicationTableDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static com.iafanasiev.parser.NumericParser.parseValueTo;

public class MultiplicationTableDTOFactory {
    static Logger logger = LoggerFactory.getLogger(MultiplicationTableDTOFactory.class);

    public static MultiplicationTableDTO buildTableParamsDTO(String type, String minStr, String maxStr, String incrementStr) {
        logger.debug("Start parsing to BigDecimal the properties and creating the dto parameters for table");

        logger.debug("Parse min: {} for type: {}", minStr, type);
        BigDecimal min = parseValueTo(minStr, type);
        logger.debug("Parse max: {} for type: {}", maxStr, type);
        BigDecimal max = parseValueTo(maxStr, type);
        logger.debug("Parse increment: {} for type: {}", incrementStr, type);
        BigDecimal increment = parseValueTo(incrementStr, type);

        return new MultiplicationTableDTO(type, min, max, increment, TypeRange.fromString(type));
    }

    private MultiplicationTableDTOFactory() {
    }
}