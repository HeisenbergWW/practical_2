package com.iafanasiev.util;

import com.iafanasiev.exception.UnsupportedDataTypeException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class TypeRangeTest {
    @ParameterizedTest
    @CsvSource({
            "shortt, 1",
            "longg, 1",
            "doublee, 1",
            "intt,1",
            "floatt,1"
    })
    void fromStringTest(String type) {
        assertThrows((UnsupportedDataTypeException.class), () -> TypeRange.fromString(type));
    }
}