package com.iafanasiev.util;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesLoaderTest {

    @Test
    void validFileTest() throws IOException {
        String fileName = "test.properties";

        Properties properties = PropertiesLoader.loadProperties(fileName);

        assertNotNull(properties);
        assertEquals("1", properties.getProperty("min"));
        assertEquals("127", properties.getProperty("max"));
        assertEquals("10", properties.getProperty("increment"));
    }

    @Test
    void fileNotFoundTest() {
        String fileName = "nonexistent.properties";

        FileNotFoundException exception = assertThrows(FileNotFoundException.class, () -> PropertiesLoader.loadProperties(fileName));
        assertEquals("Could not find properties file: " + fileName, exception.getMessage());
    }
    @Test
    void createPrivateConstructorTest() {
        assertThrows(IllegalStateException.class, PropertiesLoader::new);
    }
}