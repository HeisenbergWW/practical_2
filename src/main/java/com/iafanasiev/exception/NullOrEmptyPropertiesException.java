package com.iafanasiev.exception;

public class NullOrEmptyPropertiesException extends RuntimeException{
    public NullOrEmptyPropertiesException(String message) {
        super(message);
    }
}